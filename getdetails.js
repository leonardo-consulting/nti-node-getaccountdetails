const http = require('http');
const fs = require('fs');
var jsonQuery = require('json-query');

//console.log(result);
http.createServer((request, response) => {
    const { headers, method, url } = request;
    let body = [];
    request.on('error', (err) => {
        console.error(err);
    }).on('data', (chunk) => {
        body.push(chunk);
    }).on('end', () => {
        body = Buffer.concat(body).toString();

var accounts = {
    "accounts": [
        {
        "customerId":"LEO1",
        "accountStatus":"ACTIVE",
        "monthsActive":24,
        "currentPolicy":{
            "policyId":"NTI192C2018",
            "policyType":"Cargo Protect",
            "policyName":"Combined Cargo"
        },
        "claimHistory":[
            {"claimId":"9875",
            "customerId":"X5432",
            "claimType":"marine",
            "customerType":"simple",
            "claimComplexity":0.0,
            "submissionDate":"2010-02-15",
            "claimDate":"2010-02-13",
            "description":"loss of shipment due to storm",
            "claimAmount":15000,
            "claimStatus":"PAID"}]
        },
        {
            "customerId":"LEO2",
            "accountStatus":"ACTIVE",
            "monthsActive":13,
            "currentPolicy":{
                "policyId":"NTI193C2018",
                "policyType":"Cargo Protect",
                "policyName":"Inland Cargo"
            },
            "claimHistory":[]
            }
    ]
};

var resultPayload = jsonQuery('accounts[**][customerId=' + body + ']', {
    data: accounts
}).value;

console.log(body);

response.statusCode = 201;
response.setHeader('Content-Type', 'application/json');
// response.setHeader('User-Agent', 'PhilTest');

const responseBody = { headers, method, url, body };

response.write(JSON.stringify(resultPayload));
fs.writeFileSync('textresponse-9321.txt', JSON.stringify(resultPayload), () => {});
fs.writeFileSync('headers-9321.txt', JSON.stringify(responseBody.headers), () => {});

response.end();

    });
}).listen(8080);